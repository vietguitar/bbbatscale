import multiprocessing

bind = "0.0.0.0:8000"
chdir = "/django-project/"
workers = multiprocessing.cpu_count() * 2 + 1
max_requests = 5000
