# encoding: utf-8
import logging
from django.core.management.base import BaseCommand
from tendo.singleton import SingleInstance, SingleInstanceException

from core.models import Room, Instance, GeneralParameter
from django.db import transaction

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'perform health check on all instances'

    @transaction.non_atomic_requests
    def handle(self, *args, **options):

        try:
            SingleInstance()
            # perform health check and data collection on all known servers
            logger.info("Start checking instances stats")
            print('Performing health checks...')
            for instance in Instance.objects.all():
                with transaction.atomic():
                    instance.collect_stats()
            logger.info("Done checking instances stats")

            logger.info("Start updating max counter.")
            # update max counter
            gp = GeneralParameter.load()
            participant_current = Room.get_participants_current()
            if participant_current > gp.participant_total_max:
                gp.participant_total_max = participant_current
                gp.save()
            logger.info("Done updating max counter.")

        except SingleInstanceException as e:
            logger.error(e)
