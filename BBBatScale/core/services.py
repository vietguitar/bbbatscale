import logging

from django.conf import settings
from django.db.models import OuterRef, Subquery
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from core.event_collectors.base import EventCollectorContext
from .constants import ROOM_STATE_INACTIVE, INSTANCE_STATE_ERROR, GUEST_POLICY_ALLOW
from .models import Room, Meeting, RoomEvent, Instance, MoodleRoom
from .utils import BigBlueButton

logger = logging.getLogger(__name__)


def moderator_message(name, access_code, guest_access_code, guest_policy):
    join_url = f"{settings.BASE_URL}{reverse('join_redirect', args=[name])}"
    message = _("Meeting link: {}").format(join_url)
    ac_message = _("This room is protected by an access code: {}").format(access_code) if access_code else None
    gac_message = _("The access code for guests is: {}").format(guest_access_code) if guest_access_code else None
    gp_message = _("The room has a guest lobby enabled") if guest_policy == "ASK_MODERATOR" else None
    return '<br/>'.join(filter(None, [message, ac_message, gac_message, gp_message]))


def create_web_hook_bbb(bbb, url=settings.BASE_URL):
    logger.info("Creating web hook for {} on {} URL.".format(bbb.url, url))
    web_hook_params = {
        "callbackURL": f"{url}{reverse('callback_bbb')}"
    }
    return bbb.create_web_hook(web_hook_params)


def create_meeting_and_bbb_parameter(room, create_parameter):
    logger.info("Creating paramter for BBB Meeting.")
    try:
        if room.is_moodleroom():
            meeting = Meeting.objects.create(
                room_name=room.name,
                meeting_id=room.meeting_id,
                creator="Moodle"
            )
            create_parameter.update({
                "meta_creator": "Moodle",
                "meta_roomsmeetingid": meeting.pk,
                "meta_muteonstart": False if 'mute_on_start' not in create_parameter.keys() else create_parameter[
                    'mute_on_start'],
                "meta_allmoderator": False,
                "meta_guestpolicy": GUEST_POLICY_ALLOW,
                "meta_allowguestentry": False,
                "meta_accesscode": "None",
                "meta_accesscodeguests": "None",
                "meta_disablecam": False,
                "meta_disablemic": False,
                "meta_disablenote": False,
                "meta_disableprivatechat": False,
                "meta_disablepublicchat": False,
                "meta_allowrecording": True if create_parameter['record'] == 'true' else False,
            })
            return create_parameter
        meeting = Meeting.objects.create(
            room_name=room.name,
            meeting_id=room.meeting_id,
            creator=create_parameter['creator']
        )
        _create_parameter = {
            "name": create_parameter['name'],
            "meetingID": create_parameter['meeting_id'],
            "attendeePW": create_parameter['attendee_pw'],
            "moderatorPW": create_parameter['moderator_pw'],
            "logoutURL": settings.BASE_URL,
            "muteOnStart": "true" if create_parameter['mute_on_start'] else "false",
            "moderatorOnlyMessage": moderator_message(create_parameter['name'], create_parameter['access_code'],
                                                      create_parameter['access_code_guests'],
                                                      create_parameter['guest_policy']),
            "lockSettingsDisableCam": "true" if create_parameter['disable_cam'] else "false",
            "lockSettingsDisableMic": "true" if create_parameter['disable_mic'] else "false",
            "lockSettingsDisableNote": "true" if create_parameter['disable_note'] else "false",
            "lockSettingsDisablePublicChat": "true" if create_parameter['disable_public_chat'] else "false",
            "lockSettingsDisablePrivateChat": "true" if create_parameter['disable_private_chat'] else "false",
            "guestPolicy": create_parameter['guest_policy'],
            "record": "true" if create_parameter['allow_recording'] else "false",
            "allowStartStopRecording": "true" if create_parameter['allow_recording'] else "false",
            "meta_creator": create_parameter['creator'],
            "meta_roomsmeetingid": meeting.pk,
            "meta_muteonstart": create_parameter['mute_on_start'],
            "meta_allmoderator": create_parameter['all_moderator'],
            "meta_guestpolicy": create_parameter['guest_policy'],
            "meta_allowguestentry": create_parameter['allow_guest_entry'],
            "meta_accesscode": create_parameter['access_code'],
            "meta_accesscodeguests": create_parameter['access_code_guests'],
            "meta_disablecam": create_parameter['disable_cam'],
            "meta_disablemic": create_parameter['disable_mic'],
            "meta_disablenote": create_parameter['disable_note'],
            "meta_disableprivatechat": create_parameter['disable_private_chat'],
            "meta_disablepublicchat": create_parameter['disable_public_chat'],
            "meta_allowrecording": create_parameter['allow_recording'],
            "meta_url": create_parameter['url'],
        }
        return _create_parameter
    except KeyError:
        logger.error("Create meeting abort, because it was unable to create right parameter for meeting.")


def meeting_create(create_parameter, room=None):
    logger.info("Start creating meeting.")
    # create Webhook & Return Instance
    _room = room or Room.objects.get(meeting_id=create_parameter['meeting_id'])
    instance = _room.tenant.get_instance_for_room()
    logger.info("Selected Instance for new room: {}.".format(instance))
    bbb = BigBlueButton(instance.dns, instance.shared_secret)
    web_hook_response = create_web_hook_bbb(bbb)
    if BigBlueButton.validate_create_web_hook(web_hook_response):
        response = bbb.create(create_meeting_and_bbb_parameter(_room, create_parameter))
        logger.info("Meeting created on {} with meeting id: {} .".format(instance.dns, _room.meeting_id))
        logger.info("End creating meeting.")
        if _room.is_moodleroom():
            return response
        return BigBlueButton.validate_create(response)
    else:
        Instance.objects.filter(pk=instance.pk).update(state=INSTANCE_STATE_ERROR)
        logger.error("Meeting could not get created at {}. Server state is set to ERROR.")
        return False if not _room.is_moodleroom() else web_hook_response


def create_join_meeting_url(meeting_id, username, password):
    room = Room.objects.get(meeting_id=meeting_id)
    logger.info("INSTANCE DNS BY JOINING {}.".format(room.instance.dns))
    join_parameter = {
        "meetingID": room.meeting_id,
        "password": password,
        "fullName": username,
        "redirect": "true"
    }
    bbb = BigBlueButton(room.instance.dns, room.instance.shared_secret)
    return bbb.join(join_parameter)


def meeting_end(meeting_id, pw):
    meeting = Room.objects.get(meeting_id=meeting_id)
    return BigBlueButton(meeting.instance.dns, meeting.instance.shared_secret).end(meeting_id, pw)


def apply_room_config(meeting_id, config):
    Room.objects.filter(meeting_id=meeting_id).update(
        mute_on_start=config.mute_on_start,
        all_moderator=config.all_moderator,
        guest_policy=config.guest_policy,
        allow_guest_entry=config.allow_guest_entry,
        access_code=config.access_code,
        access_code_guests=config.access_code_guests,
        disable_cam=config.disable_cam,
        disable_mic=config.disable_mic,
        allow_recording=config.allow_recording,
        url=config.mute_on_start
    )


def reset_room(meeting_id, room_name, config):
    logger.info("Start resetting room {}".format(room_name))
    if config:
        Room.objects.filter(meeting_id=meeting_id).update(
            instance=None,
            state=ROOM_STATE_INACTIVE,
            participant_count=0,
            videostream_count=0,
            last_running=None,
            mute_on_start=config.mute_on_start,
            all_moderator=config.all_moderator,
            guest_policy=config.guest_policy,
            allow_guest_entry=config.allow_guest_entry,
            access_code=config.access_code,
            access_code_guests=config.access_code_guests,
            disable_cam=config.disable_cam,
            disable_mic=config.disable_mic,
            disable_note=config.disable_note,
            disable_private_chat=config.disable_private_chat,
            disable_public_chat=config.disable_public_chat,
            allow_recording=config.allow_recording,
            url=config.url
        )
    else:
        Room.objects.filter(meeting_id=meeting_id).update(
            instance=None,
            state=ROOM_STATE_INACTIVE,
            participant_count=0,
            videostream_count=0,
            last_running=None,
        )
    logger.info("End resetting room {}".format(room_name))


def translate_bbb_meta_data(metadata):
    try:
        return {
            "mute_on_start": True if metadata['muteonstart'] == "True" else False,
            "all_moderator": True if metadata['allmoderator'] == "True" else False,
            "guest_policy": metadata['guestpolicy'],
            "allow_guest_entry": True if metadata['allowguestentry'] == "True" else False,
            "access_code": "" if metadata['accesscode'] == "None" else metadata['accesscode'],
            "access_code_guests": "" if metadata['accesscodeguests'] == "None" else metadata['accesscodeguests'],
            "disable_cam": True if metadata['disablecam'] == "True" else False,
            "disable_mic": True if metadata['disablemic'] == "True" else False,
            "disable_note": True if metadata['disablenote'] == "True" else False,
            "disable_private_chat": True if metadata['disableprivatechat'] == "True" else False,
            "disable_public_chat": True if metadata['disablepublicchat'] == "True" else False,
            "allow_recording": True if metadata['allowrecording'] == "True" else False,
            "url": metadata['url'],
        }
    except KeyError:
        return {}


def collect_room_occupancy(room_pk):
    room = Room.objects.get(pk=room_pk)
    context = EventCollectorContext(room.event_collection_strategy)

    context.collect_events(room_pk, room.event_collection_parameters)


def room_click(room_pk):
    from django.db.models import F
    Room.objects.filter(pk=room_pk).update(click_counter=F('click_counter') + 1)


def get_rooms_with_current_next_event():
    current = RoomEvent.objects.filter(room=OuterRef('pk'), start__lte=timezone.now(),
                                       end__gte=timezone.now()).order_by('end')
    next = RoomEvent.objects.filter(room=OuterRef('pk'), start__gt=timezone.now(),
                                    end__gt=timezone.now()).order_by('start')
    return Room.objects.annotate(room_occupancy_current=Subquery(current.values('name')[:1])).annotate(
        room_occupancy_next=Subquery(next.values('name')[:1]))


def get_join_password(user, _room, join_name):
    creator = Meeting.objects.filter(room_name=_room.name).first().creator
    if _room.guest_policy == "ASK_MODERATOR" and not creator == join_name:
        password = _room.attendee_pw
    else:
        password = _room.moderator_pw if _room.all_moderator or user.groups.filter(
            name=settings.MODERATORS_GROUP).exists(
        ) or user.is_superuser or user.is_staff else _room.attendee_pw

    return password


def moodle_get_or_create_room_with_params(parameter, tenant):
    unique_name = parameter["name"]
    while Room.objects.filter(name=unique_name).exists():
        unique_name = "{}-{}".format(parameter["name"], timezone.now().time().isoformat()[-3:])
    parameter["name"] = unique_name
    return MoodleRoom.objects.get_or_create(
        name=parameter["name"],
        meeting_id=parameter["meetingID"],
        attendee_pw=parameter["attendeePW"],
        moderator_pw=parameter["moderatorPW"],
        tenant=tenant
    )
