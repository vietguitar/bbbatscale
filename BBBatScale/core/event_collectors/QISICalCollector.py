from urllib.request import urlopen as url_open
from bs4 import BeautifulSoup
import re
import logging

from core.event_collectors.SimpleICalCollector import SimpleICalCollector

logger = logging.getLogger(__name__)


class QISICalCollector(SimpleICalCollector):

    def __init__(self):
        self._necessary_keys = ["qis_id", "qis_url", "qis_encoding"]

    def collect_events(self, room_pk, parameters):
        logger.info("Start collect_events for room with pk={}".format(room_pk))

        parameters = self._evaluate_parameters(room_pk, parameters, self._necessary_keys)

        qis_ical_url = self._extract_qis_ical_url(parameters)

        # START magic code
        # don't know what it does. Just don't touch it! The Qis iCal file seems so be broken and needs cleaning up.
        raw_calendar = url_open(qis_ical_url).read().decode(parameters["qis_encoding"])
        raw_calendar = raw_calendar.replace('EXDATE;TZID=Europe/Berlin:\r\n',
                                            'EXDATE;TZID=Europe/Berlin:20200516T123000\r\n')
        raw_calendar = raw_calendar.replace(',\r\n', '\r\n')
        # END magic code

        ical_events = self._extract_events_from_raw_calendar(raw_calendar)
        room_events = self._create_room_events_from_ical_events(ical_events, room_pk)
        self._update_room_events_in_db(room_events, room_pk)

    def _extract_qis_ical_url(self, parameters):
        html_page = url_open(parameters["qis_url"] + parameters["qis_id"]).read().decode(parameters["qis_encoding"])
        soup = BeautifulSoup(html_page, features="html.parser")
        for link in soup.findAll('a', attrs={'href': re.compile("^https://")}):
            if re.search('iCalendarPlan', link.get('href')):
                qis_ical_url = link.get('href')
                break
        return qis_ical_url
