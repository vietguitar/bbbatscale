from django.db import IntegrityError
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
import uuid
from django.utils.crypto import get_random_string
from core.models import Room, HomeRoom, GeneralParameter
import logging
from django.contrib.auth.models import User
import hashlib
from django.conf import settings

logger = logging.getLogger(__name__)


@receiver(post_save, sender=Room)
def set_values_after_room_creation(instance, created, *args, **kwargs):
    if created:
        if not instance.meeting_id:
            instance.meeting_id = uuid.uuid4().hex
        if not instance.attendee_pw:
            instance.attendee_pw = get_random_string(length=10)
        if not instance.moderator_pw:
            instance.moderator_pw = get_random_string(length=10)
        instance.save()


@receiver(post_save, sender=HomeRoom)
def set_values_after_homeroom_creation(instance, created, *args, **kwargs):
    set_values_after_room_creation(instance, created, *args, **kwargs)


@receiver(post_save, sender=User)
def create_home_room_on_user_creation(instance: User, created, *args, **kwargs):
    get_or_create_home_room_for_user(instance)


@receiver(post_save, sender=GeneralParameter)
def update_home_room_on_general_parameter_change(instance: GeneralParameter, created, *args, **kwargs):
    if instance.home_room_enabled:
        if instance.home_room_tenant is None or instance.home_room_room_configuration is None:
            HomeRoom.objects.all().delete()
            msg = 'Misconfiguration! home_room_enabled has been set to {} '.format(instance.home_room_enabled) + \
                  'but home_room_tenant(={}) or '.format(instance.home_room_tenant) + \
                  'home_room_room_configuration(={}) is unset'.format(instance.home_room_room_configuration)
            logger.error(msg)
        else:
            if instance.home_room_teachers_only:
                HomeRoom.objects.exclude(owner__groups__name__contains=settings.MODERATORS_GROUP).delete()

            HomeRoom.objects.all().update(tenant=instance.home_room_tenant,
                                          config=instance.home_room_room_configuration)
            for user in User.objects.all():
                print(user)
                print(user.groups.all())
                get_or_create_home_room_for_user(user)

    elif not instance.home_room_enabled:
        HomeRoom.objects.all().delete()


def get_or_create_home_room_for_user(user: User):
    def create_room_name(user: User):
        if user.email and not HomeRoom.objects.filter(name="home-" + user.email).exists():
            name = "home-" + user.email
        else:
            name = "home-" + user.last_name + "-" + hashlib.sha1(bytes(user.username, "UTF-8")).hexdigest()[:8]
        return name

    general_parameters: GeneralParameter = GeneralParameter.load()
    user_is_teacher = user.groups.filter(name__icontains=settings.MODERATORS_GROUP).exists() or user.is_superuser

    if general_parameters.home_room_enabled and user_is_teacher or \
            not general_parameters.home_room_teachers_only and not user_is_teacher:

        try:
            HomeRoom.objects.get_or_create(
                name=create_room_name(user),
                owner=user,
                tenant=general_parameters.home_room_tenant,
                is_public=False,
                config=general_parameters.home_room_room_configuration
            )
        except IntegrityError as e:
            logger.error("Home room could not be created. " + e.args[0])
