from django.test import TestCase

import os
from core.models import Tenant, Instance, Room, RoomEvent
from core.services import collect_room_occupancy, get_rooms_with_current_next_event
from freezegun import freeze_time
from django.conf import settings
from datetime import datetime, timezone


class CollectRoomOccupancy(TestCase):

    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="FBI",
        )

        self.hda = Tenant.objects.create(
            name="h_da",
        )

        self.bbb_instance = Instance.objects.create(
            tenant=self.fbi,
            dns="bbb10.fbi.h-da.de",
            participant_count_max=10,
            videostream_count_max=2,
        )

        qiz_path = "https://qis.h-da.de/qisserver/rds?state=wplan&act=Raum&pool=Raum&P.subc=plan&raum.rgid="
        self.room_d14_0303_qis = Room.objects.create(
            tenant=self.fbi,
            instance=self.bbb_instance,
            name="D14/03.03",
            participant_count=10,
            videostream_count=5,
            event_collection_strategy="QISICalCollector",
            event_collection_parameters='{"qis_url": "' + qiz_path + '", '
                                                                     '"qis_id": "118", '
                                                                     '"qis_encoding": "UTF-8"}',
        )

        file_path = "file://" + os.getcwd() + "/core/tests/test_data/SimpleICalCollector_testdata.ics"
        self.room_d14_0204_ical = Room.objects.create(
            tenant=self.fbi,
            instance=self.bbb_instance,
            name="D14/02.04",
            participant_count=10,
            videostream_count=5,
            event_collection_strategy="SimpleICalCollector",
            event_collection_parameters='{"iCal_url": "' + file_path + '", "iCal_encoding": "UTF-8"}',
        )

        settings.EVENT_COLLECTION_SYNC_SYNC_HOURS = 24

    @freeze_time("2020-05-25 06:30:00", tz_offset=0)
    def test_collect_room_occupancy(self):
        collect_room_occupancy(self.room_d14_0204_ical.pk)

        room_events = RoomEvent.objects.filter(room=self.room_d14_0204_ical)
        self.assertEqual(len(room_events), 4)


class RoomEventsNextCurrentTest(TestCase):
    def setUp(self) -> None:
        self.fbi = Tenant.objects.create(
            name="FBI",
        )
        self.room_d14_0204 = Room.objects.create(
            tenant=self.fbi,
            name="D14/02.04"
        )
        self.room_d14_0204_event_one = RoomEvent.objects.create(
            uid="Crypto",
            room=self.room_d14_0204,
            name="Crypto",
            start=datetime(2020, 5, 25, 8, 30, tzinfo=timezone.utc),
            end=datetime(2020, 5, 25, 10, 00, tzinfo=timezone.utc)
        )

        self.room_d14_0204_event_two = RoomEvent.objects.create(
            uid="DB2",
            room=self.room_d14_0204,
            name="DB2",
            start=datetime(2020, 5, 25, 10, 15, tzinfo=timezone.utc),
            end=datetime(2020, 5, 25, 11, 45, tzinfo=timezone.utc)
        )
        self.room_d14_0204_event_three = RoomEvent.objects.create(
            uid="OOAD",
            room=self.room_d14_0204,
            name="OOAD",
            start=datetime(2020, 5, 25, 14, 15, tzinfo=timezone.utc),
            end=datetime(2020, 5, 25, 15, 45, tzinfo=timezone.utc)
        )

    @freeze_time("2020-05-25 06:30:00", tz_offset=0)
    def test_get_rooms_with_current_next_event_without_current_with_next(self):
        rooms = get_rooms_with_current_next_event()
        self.assertIsNone(rooms[0].room_occupancy_current)
        self.assertEqual(rooms[0].room_occupancy_next, "Crypto")

    @freeze_time("2020-05-25 08:30:00", tz_offset=0)
    def test_get_rooms_with_current_next_event_with_current_and_next(self):
        rooms = get_rooms_with_current_next_event()
        self.assertEqual(rooms[0].room_occupancy_current, "Crypto")
        self.assertEqual(rooms[0].room_occupancy_next, "DB2")

    @freeze_time("2020-05-25 15:30:00", tz_offset=0)
    def test_get_rooms_with_current_next_event_with_current_and_without_next(self):
        rooms = get_rooms_with_current_next_event()
        self.assertEqual(rooms[0].room_occupancy_current, "OOAD")
        self.assertIsNone(rooms[0].room_occupancy_next)

    @freeze_time("2020-05-25 16:30:00", tz_offset=0)
    def test_get_rooms_with_current_next_event_without_current_and_next(self):
        rooms = get_rooms_with_current_next_event()
        self.assertIsNone(rooms[0].room_occupancy_current)
        self.assertIsNone(rooms[0].room_occupancy_next)
