from django.conf.urls import include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path

from core.views import join_redirect, home, recording_redirect, join_redirect_deprecated

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('admin/', admin.site.urls),
    path('core/', include('core.urls')),

    path('login/', auth_views.LoginView.as_view(template_name='login.html', redirect_authenticated_user=True),
         name="login"),
    path('logout/', auth_views.LogoutView.as_view(next_page='home'), name="logout"),
    path('', home, name="home"),

    path('r/<path:room>', join_redirect, name='join_redirect'),
    path('p/<str:replay_id>', recording_redirect, name="recording_redirect"),
    path('<str:building>/<str:room>', join_redirect_deprecated),
]
