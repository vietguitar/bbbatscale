stages:
  - commit:build
  - commit:test
  - commit:analysis
  - commit:tag

include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml
  - template: License-Scanning.gitlab-ci.yml

dev_build:
  stage: commit:build
  rules:
    - when: on_success
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/docker/common/Dockerfile --destination $CI_REGISTRY_IMAGE/dev-container:$CI_COMMIT_SHA

unit_testing:
  stage: commit:test
  rules:
    - when: on_success
  image:
    name: $CI_REGISTRY_IMAGE/dev-container:$CI_COMMIT_SHA
  variables:
    POSTGRES_DB: bbbatscale
    POSTGRES_USER: bbbatscale
    POSTGRES_PASSWORD: SuperSecretPipelinePassword
    POSTGRES_HOST: postgres
    POSTGRES_PORT: 5432
    BASE_URL: localhost
    DJANGO_SECRET_KEY: SuperSecretPipelinePassword
    RECORDINGS_SECRET: SuperSecretPipelinePassword
  services:
    - name: postgres:12.3-alpine
  before_script:
    - await_postgres_connection "$POSTGRES_HOST" "$POSTGRES_DB" "$POSTGRES_USER" 10
    - cd BBBatScale
  script:
    - python manage.py test
  artifacts:
    paths:
      - BBBatScale/coverage.xml
  needs:
    - job: dev_build

flake8_checking:
  stage: commit:test
  rules:
    - when: on_success
  image:
    name: $CI_REGISTRY_IMAGE/dev-container:$CI_COMMIT_SHA
    entrypoint: [""]
  script:
    - flake8 --config=./flake8.ini --output-file=flake8Report.txt
  artifacts:
    paths:
      - flake8Report.txt
  needs:
    - job: dev_build

dependency_scanning:
  stage: commit:analysis
  allow_failure: true
  variables:
    DS_DISABLE_DIND: "true"
    DS_REMEDIATE: "false"
    DS_DEFAULT_ANALYZERS: "gemnasium-python"
  before_script:
    - apt -y install libpq-dev build-essential python3-dev libldap2-dev libsasl2-dev ldap-utils tox lcov valgrind
  needs: []

sast:
  stage: commit:analysis
  allow_failure: true
  variables:
    SAST_DISABLE_DIND: "true"
    SAST_DEFAULT_ANALYZERS: "bandit, secrets"
  needs: []

license_scanning:
  stage: commit:analysis
  needs: []

sonarqube:
  stage: commit:analysis
  rules:
    - if: $SONARQUBE_TOKEN == null || $CI_COMMIT_BRANCH == null
      when: never
    - when: on_success
  image:
    name: sonarsource/sonar-scanner-cli:4.2
    entrypoint: [""]
  script:
    - sonar-scanner
      -Dsonar.projectKey=bbbatscale
      -Dsonar.sources=BBBatScale
      -Dsonar.host.url=https://bbbatscale.sonar.fbi.h-da.de
      -Dsonar.login=$SONARQUBE_TOKEN
      -Dsonar.python.coverage.reportPaths=BBBatScale/coverage.xml
      -Dsonar.cpd.exclusions=**/settings/**
      -Dsonar.coverage.exclusions=**/migrations/**
      -Dsonar.exclusions=**/staticfiles/**,**/static/**
      -Dsonar.python.pylint.reportPath=flake8Report.txt
      -Dsonar.branch.name=$CI_COMMIT_BRANCH
  needs:
    - job: unit_testing
      artifacts: true
    - job: flake8_checking
      artifacts: true

tag_container_image:
  stage: commit:tag
  only:
    - master
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  script:
    - crane auth login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - crane cp "$CI_REGISTRY_IMAGE/dev-container:$CI_COMMIT_SHA" "$CI_REGISTRY_IMAGE:latest"
