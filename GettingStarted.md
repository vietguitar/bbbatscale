# Getting Started

The following steps describe how to get BBB@Scale up and running.
The sections are split into: prerequisites, installation, configuration and administration.

If you just want to play around and get first impressions of the tool check out our [Demo](http://demo.bbbatscale.de)
(Username=admin | Password=bbbATscale). The Demo resets its configuration at 00:00 CEST.

If you have any questions which are not answered by this ReadMe or [Getting Started Guide ](GettingStarted.md), feel free to send us a mail at mail@bbbatscale.de.
If you want send us an encrypted email, use the pgp-key under
([PGP-Key](mailAtBBBatScale.de_public.pgp)).

## Architecture & Overview

The following architectural overview describes the basic components and procedures involved in a BBB@Scale setup.
![Architecture](/Documentation/architecture.png?raw=true 'Architectural overview')

1. A users visits the BBB@Scale web page and selects a room.
2. BBB@Scale schedules a room on a appropriate BBB instance whether it is on premise or in the cloud.
3. BBB@Scale redirects the user to the desired room, after its creation is confirmed.

## Prerequisites

For a minimal working installation of BBB@Scale you need

- 1x Server (Physical, Virtual) with Linux (we use Ubuntu 20.04) and Docker enabled
- 1x BBB Server with [bbb-webhooks](https://docs.bigbluebutton.org/dev/webhooks.html) enabled

### Example setup

Check out our [example setup ](/Documentation/setup_example.md) to get an idea how we operate our productive BBB
@Scale setup

## Installation (Standalone, Minimal)

We are using a VM with Ubuntu 20.04. Please make sure that it is reachable via TCP 80 and you have a working docker setup.
Digital Ocean has a great [tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-de) on how to setup docker.

**Step 1**
Navigate to the path where you want the project to be located. In our case, we chose "/".

```shell
cd /
```

**Step 2**
Clone the repository

```shell
git clone git@gitlab.com:bbbatscale/bbbatscale.git
```

**Step 3**
Rename the env files for the docker containers inside /bbbatscale/env/


```shell
mv /bbbatscale/env/django.env.dummy /bbbatscale/env/django.env
mv /bbbatscale/env/postgres.env.dummy /bbbatscale/env/postgres.env
```

**Step 4**
Create custom and random secrets for DJANGO_SECRET_KEY, DUMMY_RECORDINGS_SECRET and set the correct BASE_URL
```shell
nano /bbbatscale/env/django.env
```
Example
```
DJANGO_SETTINGS_MODULE=BBBatScale.settings.production
BASE_URL=http://demo.bbbatscale.de
DJANGO_SECRET_KEY=ghjfh.....4446vhjs
RECORDINGS_SECRET=gfj4HGd.....dhjbsjhbdjhk
POSTGRES_HOST=postgressql
```

**Step 5**
Define credentials and database name for your postgres connection by editing POSTGRES_DB, POSTGRES_USER, POSTGRES_PASSWORD
```shell
nano /bbbatscale/env/postgres.env
```

Example
```
POSTGRES_DB=bbbatscaledb
POSTGRES_USER=bbbatscaleuser
POSTGRES_PASSWORD=GHD3Jdbdb8hj
```

**Step 6**
Run the containers

```shell
cd /bbbatscale
docker-compose up -d
```

**Step 7**
Create an admin user

```shell
docker exec -it django ./manage.py createsuperuser
```

**Step 8**
Add the following lines to your crontab.

```shell
*/1 * * * * docker exec -i django /usr/local/bin/python manage.py collect_instances_stats
*/3 * * * * docker exec -i django /usr/local/bin/python manage.py house_keeping
```

**Step 9**
Open your browser, navigate to http://*Your IP\* and enjoy

**Step 10** - Optional LDAP
LDAP authentication requires a
[django-auth-ldap](https://django-auth-ldap.readthedocs.io/en/latest/index.html)
configuration file describing your local LDAP setup.  To enable LDAP
authentication create a file named `ldap_config.py` in the Django settings
directory (see
[here](https://django-auth-ldap.readthedocs.io/en/latest/example.html) for an
example).

When using docker-compose, make the locally stored file available to the
`django` container at `/django-project/BBBatScale/settings/ldap_config.py`
using an override file or by editing `docker-compose.yml`.

See the [django-auth-ldap
docs](https://django-auth-ldap.readthedocs.io/en/latest/example.html) for how
to grant admin and staff privileges to existing LDAP groups.
Use the BBBATSCALE_MODERATORS_GROUP environment variable to specify the LDAP
group which grants members moderator privileges within BBB@Scale.

## Installation (OpenShift, Kubernetes)

This part will be ready soon.

## Update
Just stop docker, update the project files via git pull, rebuild the containers and restart them again.
```shell
cd /bbbatscale
docker-compose stop
git pull
docker-compose build
docker-compose up -d
```

## Administration

After getting the installation up and running, you are ready to fill up BBB@Scale with your specific settings.

Follow the steps below:

- Create a Tenant
  We assign and schedule BBB-Servers and rooms specific to one tenant. For this, you need to have at least one tenant in your setup.
  - Name: You need to provide a unique name, which will be shown in the rooms portal later on.
  - Scheduling Strategy: You have a choice between "Least participants" and "Least utilization". Default is set to "Least utilization". Least participants schedules based on the number of users in a meeting. Least utilization schedules based on a calculation of system usage (Audio and video users).

### Tenants screen. Listing all available tenants and create new ones

![Tenants](/Documentation/Screenshots/7.create_tenant.png?raw=true 'Tenants')

- Create Room Types
  You need to predefine at least one Room Type.
  - Name: You need to provide a unique name, which will be shown in the rooms portal later on.

### Room Types screen. Listing all available Room Types and create new ones

![RoomTypes](/Documentation/Screenshots/8.create_roomstypes.png?raw=true 'RoomTypes')

- Register server
  You need to have BigBlueButton servers installed in your deployment. Take a look at [bbb-install]https://github.com/bigbluebutton/bbb-install. We recommend a small BBB Setup without the need to install Greenlight.
  - DNS: Insert your BBB Server DNS here.
  - Tenant: Select the apropriate tenant previously created.
  - Datacenter: This is optional.
  - Shared Secret: Insert the output of the section named Secret `bbb-conf --secret` here.
  - Max participants: Is used for calculation of utilization, should represent the capability of our BBB server. We will not set a hard limit in BBB.
  - Max videostreams: Is used for calculation of utilization, should represent the capability of our BBB server. We will not set a hard limit in BBB.

### Server screen. Listing all available BBB-Server and import new ones

![ServerScreen](/Documentation/Screenshots/9.register_bbbserver.png?raw=true 'ServerScreen')

- Create the needed Rooms
  Feel free to name them as you wish. For our needs, we named a lot of rooms based on existing physical buildings to facilitate virtual and real life scenarios.
  - Tenant and Name: Defines the tenant and name.
  - Configuration: Defines the preset for the specific room.
  - Public: Mark this if the room should be visible on the startpage.
  - Comment private: For an internal view of comments in Rooms.
  - Comment public: For an external view of comments in Home.

### Rooms screen. Listing all available Rooms and create new ones

![Rooms](/Documentation/Screenshots/10.create_rooms.png?raw=true 'Rooms')

- Import / Export
